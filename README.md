# Unam DH Menu Documentation #

The following is a documentation on how the Unam DH Menu system is used.

### Requirements ###

* Web app: Chrome browser | Mozilla browser
* Mobile App: Android | iOS | Windows phone
* Internet connection

### How do I get set up? ###

Mobile app
* Download and install the apk from this repo
* Use the app to view the DH Menu and make bookings

Web app
* Open the Web app using a supported browser
* Login with valid credentials to be able to view booked meals


### Functionality ###

* Backend database -> Firebase


### Developers ###

* Tatenda Audrey Chanakira
* Sanette Shetekela
* Loide Kephas
* Noble Mutoko
* Ben Handumo