import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Menu } from '../model/menu/menu.model';

@Injectable()
export class MenuListService {

    private menuListRef = this.db.list<Menu>('menu-list');

    constructor(private db: AngularFireDatabase) { }

    getMenuList() {
        return this.menuListRef;
    }

    addMenu(menu: Menu) {
        return this.menuListRef.push(menu);
    }

    updateMenu(menu: Menu) {
        return this.menuListRef.update(menu.key, menu);
    }

    removeMenu(menu: Menu) {
        return this.menuListRef.remove(menu.key);
    }
}
