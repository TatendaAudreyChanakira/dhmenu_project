export interface Menu {
    key?: string;
    studentNumber: string;
    phoneNumber: string;
    breakfast: string;
    lunch: string;
    dinner:string;
}
