import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {BreakfastPage} from '../breakfast/breakfast';
import {LunchPage} from '../lunch/lunch';
import {DinnerPage} from '../dinner/dinner';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

breakfastPage = BreakfastPage;
lunchPage = LunchPage;
dinnerPage = DinnerPage

  constructor(public navCtrl: NavController) {

  }

}
