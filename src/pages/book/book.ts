import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Menu } from '../../model/menu/menu.model';
import { MenuListService } from '../../services/menu-list.service';
import {Observable} from 'rxjs/Rx';
import { NgModule } from '@angular/core'
import { IonicPageModule } from 'ionic-angular/module';

/**
 * Generated class for the BookPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book',
  templateUrl: 'book.html',
})

@NgModule({
    declarations: [BookPage],
    imports: [IonicPageModule.forChild(BookPage)]
})

export class BookPage {
menuList: Observable<Menu[]>

  menu: Menu = {
  studentNumber: '',
  phoneNumber: '',
  breakfast: '',
  lunch: '',
  dinner: ''
};

  constructor(public navCtrl: NavController, public navParams: NavParams, private menuListService: MenuListService, private alertCtrl: AlertController) {

    this.menuList = this.menuListService.getMenuList()
    .snapshotChanges()
    .map(
    changes => {
      return changes.map(c => ({
        key: c.payload.key, ...c.payload.val()
      }))
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookPage');
  }

  addMenu(menu: Menu) {
    this.menuListService.addMenu(menu).then(ref => {
    //  this.navCtrl.setRoot('HomePage');
    let alert = this.alertCtrl.create({
      title: 'DH Menu Book',
      subTitle: 'You have successfully booked your meals. Thank you.',
      buttons: ['Ok']
    });
    alert.present();
    })
  }


}
