import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { FIREBASE_CONFIG } from './firebase.credentials';
import { MenuListService } from '../services/menu-list.service';
import { FormsModule } from '@angular/forms';


import { BookPage } from '../pages/book/book';
import { KioskPage } from '../pages/kiosk/kiosk';
import { HomePage } from '../pages/home/home';
import { DinnerPage } from '../pages/dinner/dinner';
import { BreakfastPage } from '../pages/breakfast/breakfast';
import { LunchPage } from '../pages/lunch/lunch';
import { TabsPage } from '../pages/tabs/tabs';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';



@NgModule({
  declarations: [
    MyApp,
    BookPage,
    KioskPage,
    HomePage,
    BreakfastPage,
    DinnerPage,
    LunchPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BookPage,
    KioskPage,
    HomePage,
    BreakfastPage,
    DinnerPage,
    LunchPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MenuListService
  ]
})
export class AppModule {}
